class ChatManager {
  constructor() {
    this.roomIdCounter = 0;
    this.userIdCounter = 0;

    this.rooms = {};
    this.users = {};
  }

  newRoomId() {
    return ++this.roomIdCounter;
  }

  newUserId() {
    return ++this.userIdCounter;
  }

  newUser(user) {
    const id = this.newUserId();

    return {
      id,
      username: user.username,
      email: user.email,
    }
  }

  newRoom(user) {
    const id = this.newRoomId();

    return {
      id,
      users: {
        [user.id]: user,
      },
      messages: [],
    };
  }

  newMessage(message, user, roomId) {
    return {
      message,
      user,
      roomId,
    }
  }

  addUserToRoom(user, roomId) {
    this.rooms[roomId].users[user.id] = user;
  }

  addMessageToRoom(message, roomId) {
    this.rooms[roomId].messages.push(message);
  }

  saveUser(user) {
    this.users[user.id] = user;
  }

  saveRoom(room) {
    this.rooms[room.id] = room;
  }

  createRoom(user) {
    const roomUser = this.newUser(user);
    const room = this.newRoom(roomUser);

    this.saveRoom(room);
    this.saveUser(roomUser);

    return { roomId: room.id, user: roomUser };
  }

  joinRoom(roomId, user) {
    if (this.rooms[roomId]) {
      const roomUser = this.newUser(user);

      this.saveUser(roomUser);

      const otherUsers = this.getRoomUsers(roomId);

      const messages = this.getRoomMessages(roomId);

      this.addUserToRoom(roomUser, roomId);

      return { roomId, user: roomUser, userList: otherUsers, messages };

    } else {
      console.log('no such room')
    }
  }

  saveMessage(text, userId, roomId) {
    const user = this.getUser(userId);
    const message = this.newMessage(text, user, roomId);

    this.addMessageToRoom(message, roomId);

    return message;
  }

  getUser(userId) {
    return this.users[userId];
  }

  getRoomUsers(roomId) {
    return Object.values(this.rooms[roomId].users);
  }

  getRoomMessages(roomId) {
    return this.rooms[roomId].messages;
  }

  deleteRoom(roomId) {
    const roomUsers = Object.keys(this.rooms[roomId].users);

    this.deleteUsers(roomUsers);

    return delete this.rooms[roomId];
  }

  deleteUsers(usersIds) {
    for (const userId of usersIds) {
      delete this.users[userId];
    }
  }
}

module.exports = new ChatManager();