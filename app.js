const express = require('express');
const port = 3000;
const socket = require('socket.io');
const chatManager = require('./chat-manager');

const app = express();

app.use((req, res, next) => {
    res.append('Access-Control-Allow-Origin', 'http://localhost:4200');
    res.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.append("Access-Control-Allow-Headers", "Origin, Accept,Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
    res.append('Access-Control-Allow-Credentials', true);
    next();
});

app.get('/', (req, res, next) => {
    res.send('Hello World');
});

const server = app.listen(port, () => {
    console.log("Server started on port " + port + "...");
});

const io = socket.listen(server);

io.sockets.on('connection', (socket) => {
    socket.on('create', ({ user }) => {

        const data = chatManager.createRoom(user);

        socket.emit('created', data);
    });

    socket.on('join', ({ user, roomId }) => {

        const data = chatManager.joinRoom(roomId, user);

        io.sockets.emit('joined', data);
    });

    socket.on('message', ({ userId, message, roomId }) => {
        const data = chatManager.saveMessage(message, userId, roomId);

        io.sockets.emit('message', data);
    });

    socket.on('typing', ({ userId, roomId }) => {
        const user = chatManager.getUser(userId);

        socket.broadcast.emit('typing', { user, roomId });
    });

    socket.on('destroy', ({ roomId }) => {

        const destroyed = chatManager.deleteRoom(roomId);

        if (destroyed) {
            io.sockets.emit('destroyed', { roomId });

        } else {
            console.log("can't destroy room", roomId);
        }
    });
});